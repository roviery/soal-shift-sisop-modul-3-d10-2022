#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <pthread.h>
#include <unistd.h>
#include <wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define PORT 8080


int receiver(int socket, char *filename);

int main (int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    int numbers[100];

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    valread = read(new_socket, buffer, 1024);
    receiver(new_socket, "hartakarun.zip");
    close(new_socket);

    return 0;
}

int receiver(int socket, char *filename){
    char buffer[1024] = {0}, fpath[1024];
    int fileSize = 0;
    int bufferOnset();
   
    strcpy(fpath, "./");
    strcat(fpath, filename);
    FILE *read_file = fopen(fpath, "wb");

    if (read_file == NULL)
    {
        printf("File not found: %s\n", filename);
        return -1;
    }
    else {
        printf("File from Client: %s\n", filename);
        bufferOnset(buffer, 1024);
        fileSize = recv(socket, buffer, 1024, 0);

        while (fileSize > 0)
        {
            int size_write = fwrite(buffer, sizeof(char), fileSize, read_file);
            if (size_write < fileSize)
            {
                printf("Server failed to keep\n");
                return 0;
            }

            bufferOnset(buffer, 1024);

            if (fileSize == 0)
            {
                break;
            }
        }
    }
    fclose(read_file);
}
