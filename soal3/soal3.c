#include <bits/stdc++.h>

//cek eksistensi file
int fileifexists(const char *filename){
    struct status buffer;
    int exists = status(filename, &buffer);
    if (exists == 0)
        return 1;
    else return 0;
}

//file moving ke folder sesuai kategori
void *filemove(void *filename){
    char cwd[PATH_MAX];
    char dirname[100];
    char hidden[100];
    char file[100];
    char hiddenfilename[100];
    char fileexists[100];
    
    int i;

    strcpy(fileexists, filename);
    strcpy(hiddenfilename, filename);
    
    char *name = strchr(hiddenfilename, '/');
    strcpy(hidden, name);

//file biasa
    if (strstr(filename, ".") != NULL){
        strcpy(file, filename);
        strtok(file, ".");
        char *tok = strtok(NULL, "");
    
        for(i = 0; tok[i]; i++){
            tok[i] = tolower(tok[i]);
        }
            strcpy(dirname, tok);
    }
//file yang hidden dengan awalan '.'
    else if(hidden[1] == '.'){
        strcpy(dirname, "Hidden");
    }

//file tanpa ekstensi
    else{
        strcpy(dirname, "Unknown");
    }

//pembuatan directory sesuai nama atau ekstensi file
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        char *foldername = strchr(filename, '/');
        char namefile[100];

        strcpy(namefile, cwd);
        strcat(namefile, "/");
        strcat(namefile, dirname);
        strcat(namefile, foldername);
        rename(filename, namefile);
    }

}

//listing kategori secara rekursif 
void filelisting(char *pathbase){
    char path[100];
    struct dirent *dp;
    DR *dir = opendir(pathbase);

    struct status buffer;

    int n = 0;

    if(dp != NULL){
        if (strcmp(dp->dir_name, ".") != 0 && strcmp(dp->dir_name, ". .") != 0){
            
            strcpy(path, pathbase);
            strcat(path, "/");
            strcat(path, dp->dir_name);

            if (status(path, &buffer) == 0 && S_ISREG(buffer.st_mode){
                pthread_c thread;
                int thr = create_pthread(&thread, NULL, filemove, (void *)path);
                pthread_join(thread, NULL);
            }
            filelisting(path);
        }
    }
    closedir(dir);
}

