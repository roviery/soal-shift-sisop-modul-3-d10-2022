#define _GNU_SOURCE
#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <stdlib.h>

void forking_v(char **arg)
{
    pid_t pid; int status;

    char addr[10];
    strcpy(addr, "/bin/");
    strcat(addr, arg[0]);

    pid = fork();
    if(pid == 0){
        execv(addr, arg);
    }
    else {
        while ((wait(&status)) > 0);
    }
}

// Kebutuhan Soal 1a
void *download_zip(void *arg);
pthread_t dzip_thread[2];
struct dzip_arg { char link[80]; char name[10]; char name2[10];};

// Kebutuhan Soal 1b
void *decode(void *arg);
pthread_t dcd_thread[2];
char type_quote[10]; 
char type_music[10];

// Kebutuhan Soal 1c
void *move(void *arg);
pthread_t mv_thread[2];

// Kebutuhan Soal 1e
void *unzip_hasil(void *arg);
void *buat_no(void *arg);
pthread_t e_thread[2];

int main()
{
    // Soal 1a
    struct dzip_arg dzip_msgs1;
    struct dzip_arg dzip_msgs2;

    strcpy(dzip_msgs1.link, "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt");
    strcpy(dzip_msgs2.link, "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1");
    strcpy(dzip_msgs1.name, "quote");
    strcpy(dzip_msgs2.name, "music");
    strcpy(dzip_msgs1.name2, "quote_tmp");
    strcpy(dzip_msgs2.name2, "music_tmp");

    int dwnld_err1 = pthread_create(&(dzip_thread[0]), NULL, &download_zip, (void*) &dzip_msgs1);
    if(dwnld_err1 != 0) { printf("\n can't create thread : [%s]",strerror(dwnld_err1)); }

    int dwnld_err2 = pthread_create(&(dzip_thread[1]), NULL, &download_zip, (void*) &dzip_msgs2);
    if(dwnld_err2 != 0) { printf("\n can't create thread : [%s]",strerror(dwnld_err2)); }

    pthread_join(dzip_thread[0], NULL);
	pthread_join(dzip_thread[1], NULL);

    // Soal 1b
    strcpy(type_quote, "quote");
    strcpy(type_music, "music");

    int dcd_err1 = pthread_create(&(dcd_thread[0]), NULL, &decode, (void*) &type_quote);
    if(dcd_err1 != 0) { printf("\n can't create thread : [%s]",strerror(dcd_err1)); }

    int dcd_err2 = pthread_create(&(dcd_thread[1]), NULL, &decode, (void*) &type_music);
    if(dcd_err2 != 0) { printf("\n can't create thread : [%s]",strerror(dcd_err2)); }

    pthread_join(dcd_thread[0], NULL);
    pthread_join(dcd_thread[1], NULL);

    // Soal 1c
    char *arg_hasil[] = {"mkdir", "hasil", (char *) 0 };
    forking_v(arg_hasil);

    int mv_err1 = pthread_create(&(mv_thread[0]), NULL, &move, (void*) &type_quote);
    if(mv_err1 != 0) { printf("\n can't create thread : [%s]",strerror(mv_err1)); }

    int mv_err2 = pthread_create(&(mv_thread[1]), NULL, &move, (void*) &type_music);
    if(mv_err2 != 0) { printf("\n can't create thread : [%s]",strerror(mv_err2)); }

    pthread_join(mv_thread[0], NULL);
    pthread_join(mv_thread[1], NULL);

    // Soal 1d
    char *arg_zip[] = {"zip", "-q", "-r", "--password", "mihinomenestzahrafayya", "hasil.zip", "hasil", (char *) 0 };
    forking_v(arg_zip);

    char *arg_rm[] = {"rm", "-rf", "hasil", (char *) 0 };
    forking_v(arg_rm);

    // Soal 1e
    int e_err1 = pthread_create(&(e_thread[0]), NULL, &unzip_hasil, NULL);
    if(e_err1 != 0) { printf("\n can't create thread : [%s]",strerror(e_err1)); }

    int e_err2 = pthread_create(&(e_thread[1]), NULL, &buat_no, NULL);
    if(e_err2 != 0) { printf("\n can't create thread : [%s]",strerror(e_err2)); }

    pthread_join(e_thread[0], NULL);
    pthread_join(e_thread[1], NULL);

    remove("hasil.zip");
    char *arg_zip2[] = {"zip", "-q", "-r", "--password", "mihinomenestzahrafayya", "hasil.zip", "no.txt", "hasil", (char *) 0 };
    forking_v(arg_zip2);
    forking_v(arg_rm);
    remove("no.txt");
}

void *download_zip(void *arg) 
{
    struct dzip_arg *arguments = arg;

    printf("Downloading %s.zip...\n", arguments->name);
    char *args1[] = {"wget", "-P","./", "-o-", "-q", arguments->link, "-O", arguments->name2, (char *) 0 };
    forking_v(args1);

    printf("Making directory %s...\n", arguments->name);
    char *args2[] = {"mkdir", arguments->name, (char *) 0 };
    forking_v(args2);

    printf("Extracting %s.zip...\n", arguments->name);
    char *args3[] = {"unzip", "-q", arguments->name2, "-d", arguments->name, (char *) 0 };
    forking_v(args3);

    if (remove(arguments->name2) == 0){ printf("Successfully deleted %s.zip.\n", arguments->name); };
}

void *decode(void *arg)
{
    char *arguments = arg;

    char str1[10];
    char filename[10];

    if(arguments[0] == 'q') 
    {
        strcpy(str1, "quote/q");
        strcpy(filename, "quote.txt");
    }
    else if(arguments[0] == 'm') 
    {
        strcpy(str1, "music/m");
        strcpy(filename, "music.txt");
    }

    for(int i = 1; i <= 9; i++)
    {
        char *num;
        char path[30];

        if (asprintf(&num, "%d", i) == -1) {
            perror("asprintf");
        } else {
            strcat(strcpy(path, str1), num);
            strcat(path, ".txt");
            printf("%s\n", path);
            free(num);
        }

        printf("Decoding quote %d...\n", i);


        // Piping
        int des_p[2];
        if(pipe(des_p) == -1) {
            perror("Pipe failed");
            exit(1);
        }

        if(fork() == 0)            
        {
            close(STDOUT_FILENO);  
            dup(des_p[1]);         
            close(des_p[0]);      
            close(des_p[1]);

            char *args[] = {"base64", "--decode", path, (char *) 0 };
            execvp("base64", args);
            perror("execvp of ls failed");
            exit(1);
        }

        if(fork() == 0)            
        {
            close(STDIN_FILENO);   
            dup(des_p[0]);  
            close(des_p[0]);       
            close(des_p[1]);       

            char arr[100];
            scanf("%[^\n]s", arr);

            if(i == 1)
            {
                FILE *fp = fopen(filename, "w");
                fprintf(fp, "%s\n", arr);
                fclose(fp);
            }
            else 
            {
                FILE *fp = fopen(filename, "a");
                fprintf(fp, "%s\n", arr);
                fclose(fp);
            }
            exit(1);
        }

        close(des_p[0]);
        close(des_p[1]);
        wait(0);
        wait(0);
    }

    char *arg_rm[] = {"rm", "-rf", arguments, (char *) 0 };
    forking_v(arg_rm);
}

void *move(void *arg)
{
    char *arguments = arg;
    strcat(arguments, ".txt");

    char *arg_move[] = {"mv", arguments, "hasil" ,(char *) 0 };
    forking_v(arg_move);
}

void *unzip_hasil(void *arg)
{
    char *args[] = {"unzip", "-q", "-P", "mihinomenestzahrafayya", "hasil", (char *) 0 };
    forking_v(args);
}

void *buat_no(void *arg)
{
    FILE *fp = fopen("no.txt", "w");
    fprintf(fp, "No");
    fclose(fp);
    pthread_exit(NULL);
}
