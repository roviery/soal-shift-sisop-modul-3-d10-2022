# Soal Shift Modul 3 - D10
Penyelesaian Soal Shift Modul 3 Sistem Operasi 2022\
Kelompok D10
* Halyusa Ard Wahyudi - 5025201088
* Zahra Fayyadiyati - 5025201133
* Nathanael Roviery - 5025201258

---
## Table of Contents
* [Soal 1](#soal-1)
    * [Soal 1.a](#soal-1a)
    * [Soal 1.b](#soal-1b)
    * [Soal 1.c](#soal-1c)
    * [Soal 1.d](#soal-1d)
    * [Soal 1.e](#soal-1e)
* [Soal 2](#soal-2)
    * [Soal 2.a](#soal-2a)
    * [Soal 2.b](#soal-2b)
    * [Soal 2.c](#soal-2c)
    * [Soal 2.d](#soal-2d)
    * [Soal 2.e](#soal-2e)
    * [Soal 2.f](#soal-2f)
    * [Soal 2.g](#soal-2g)
* [Soal 3](#soal-3)
    * [Soal 3.a](#soal-3a)
    * [Soal 3.b](#soal-3b)
    * [Soal 3.c](#soal-3c)
    * [Soal 3.d](#soal-3d)
    * [Soal 3.e](#soal-3e)
#  Soal 1
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/tree/master/soal1)

**Deskripsi:**
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

## Soal 1.a
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/tree/master/soal1)

**Deskripsi:**
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

**Pembahasan:**
Untuk memudahkan pengerjaan soal, dibuat sebuah fungsi bernama `forking_v` yang berfungsi untuk melakukan forking (membuat _child process_), melakukan execv dari argumen yang di-_passing_-kan ke fungsi tersebut dan menunggu hingga _child process_ selesai dijalankan.
```c
void forking_v(char **arg)
{
    pid_t pid; int status;

    char addr[10];
    strcpy(addr, "/bin/");
    strcat(addr, arg[0]);

    pid = fork();
    if(pid == 0){
        execv(addr, arg);
    }
    else {
        while ((wait(&status)) > 0);
    }
}
```
Untuk dapat menjawab soal 1a, dibuat sebuah fungsi `download_zip()` yang akan dipanggil ketika membuat thread, variabel thread dan struct `dzip_arg` yang akan digunakan untuk melakukan passing argumen ke fungsi `download_zip()` yang dipanggil dengan thread.
```c
// Kebutuhan Soal 1a
void *download_zip(void *arg);
pthread_t dzip_thread[2];
struct dzip_arg { char link[80]; char name[10]; char name2[10];};

int main()
{
    // Soal 1a
    struct dzip_arg dzip_msgs1;
    struct dzip_arg dzip_msgs2;

    strcpy(dzip_msgs1.link, "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt");
    strcpy(dzip_msgs2.link, "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1");
    strcpy(dzip_msgs1.name, "quote");
    strcpy(dzip_msgs2.name, "music");
    strcpy(dzip_msgs1.name2, "quote_tmp");
    strcpy(dzip_msgs2.name2, "music_tmp");

    int dwnld_err1 = pthread_create(&(dzip_thread[0]), NULL, &download_zip, (void*) &dzip_msgs1);
    if(dwnld_err1 != 0) { printf("\n can't create thread : [%s]",strerror(dwnld_err1)); }

    int dwnld_err2 = pthread_create(&(dzip_thread[1]), NULL, &download_zip, (void*) &dzip_msgs2);
    if(dwnld_err2 != 0) { printf("\n can't create thread : [%s]",strerror(dwnld_err2)); }

    pthread_join(dzip_thread[0], NULL);
	pthread_join(dzip_thread[1], NULL);
}
```
Pada fungsi `download_zip()`, pertama fungsi akan melakukan casting void ke `struct dzip_arg` dari argumen yang diterima. Setelah itu, fungsi akan melakukan download zip dari link yang ada pada variabel struct arguments dengan bantuan fungsi `forking_v()`. Lalu, fungsi akan membuat directory dan mengekstrak zip yang pula dengan bantuan dari variabel struct yang telah dipassing ke fungsi tersebut dan mengeksekusinya dengan bantuan fungsi `forking_v()`. Setelah itu, fungsi akan menghapus file .zip yang telah diekstrak.
```c
void *download_zip(void *arg) 
{
    struct dzip_arg *arguments = arg;

    printf("Downloading %s.zip...\n", arguments->name);
    char *args1[] = {"wget", "-P","./", "-o-", "-q", arguments->link, "-O", arguments->name2, (char *) 0 };
    forking_v(args1);

    printf("Making directory %s...\n", arguments->name);
    char *args2[] = {"mkdir", arguments->name, (char *) 0 };
    forking_v(args2);

    printf("Extracting %s.zip...\n", arguments->name);
    char *args3[] = {"unzip", "-q", arguments->name2, "-d", arguments->name, (char *) 0 };
    forking_v(args3);

    if (remove(arguments->name2) == 0){ printf("Successfully deleted %s.zip.\n", arguments->name); };
}
```

## Soal 1.b
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/tree/master/soal1)

**Deskripsi:**
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

**Pembahasan:**
Sama seperti pada soal 1a, pertama akan disediakan terlebih dulu kebutuhan untuk thread pada 1b, yakni fungsi `decode()` dan array of char `type_quote` dan `type_music`. Setelah itu, pada `main()`, array of char `type_quote` akan diisi dengan "quote" dan array of char `type_music` akan diisi dengan "music". Kedua array of char ini digunakan untuk membantu fungsi `decode()` menentukan apakah dia berupa quote atau music. Setelah itu, thread dibuat dengan melemparkannya ke fungsi `decode()` dengan argumen `type_quote` untuk thread pertama dan `type_music` untuk thread kedua.
```c
// Kebutuhan Soal 1b
void *decode(void *arg);
pthread_t dcd_thread[2];
char type_quote[10]; 
char type_music[10];

int main()
{
    // Soal 1b
    strcpy(type_quote, "quote");
    strcpy(type_music, "music");

    int dcd_err1 = pthread_create(&(dcd_thread[0]), NULL, &decode, (void*) &type_quote);
    if(dcd_err1 != 0) { printf("\n can't create thread : [%s]",strerror(dcd_err1)); }

    int dcd_err2 = pthread_create(&(dcd_thread[1]), NULL, &decode, (void*) &type_music);
    if(dcd_err2 != 0) { printf("\n can't create thread : [%s]",strerror(dcd_err2)); }

    pthread_join(dcd_thread[0], NULL);
    pthread_join(dcd_thread[1], NULL);
}
```
Pada fungsi `decode()`, pertama fungsi akan mengecek argumen yang diberikan, apakah dia berupa "quote" atau "music". Pada proses ini pula, fungsi akan menyimpan path dari file.txt yang akan diisi dengan hasil decode di dalam array of char `filename`. Selanjutnya, fungsi akan membuat path dari .txt yang akan didecode pada array of char `path`. Hal ini dilakukan dengan bantuan `asprintf()` yang berfungsi untuk melakukan append suatu bilangan int ke dalam suatu array of char dalam bentuk bilangan yang dimaksud. Misalnya, pada for loop, apabila kini sedang berada pada loop ke-3 dengan jenis music, maka akan dilakukan append "3" ke dalam `path` sehingga isi dari `path` padda akhirnya menjadi "music/m3.txt".
```c
void *decode(void *arg)
{
    char *arguments = arg;

    char str1[10];
    char filename[10];

    if(arguments[0] == 'q') 
    {
        strcpy(str1, "quote/q");
        strcpy(filename, "quote.txt");
    }
    else if(arguments[0] == 'm') 
    {
        strcpy(str1, "music/m");
        strcpy(filename, "music.txt");
    }

    for(int i = 1; i <= 9; i++)
    {
        char *num;
        char path[30];

        if (asprintf(&num, "%d", i) == -1) {
            perror("asprintf");
        } else {
            strcat(strcpy(path, str1), num);
            strcat(path, ".txt");
            printf("%s\n", path);
            free(num);
        }

        printf("Decoding quote %d...\n", i);

```
Selanjutnya, `decode()` akan melakukan pembuatan dua child process yang mana di antara kedua child process tersebut akan dibuat sebuah pipe. Pipe ini dibuat dengan menutup `STDOUT_FILENO` pada child process pertama dan `STDIN_FILENO` pada child process kedua. Setelah itu, file descriptor yang sekarang ada akan diduplikasi menggunakan `dup()` kepada pipe `des_p` (duplikasi write end dari pipe `des_p` pada child process pertama kepada `STDOUT_FILENO` yang ditandai dengan indeks 1, dan duplikasi read end dari pipe `des_p` pada child kedua kepada `STDIN_FILENO` yang ditandai dengan indeks 0) dan kemudian akan ditutup semua aliran input dan output yang ada pada kedua child process tersebut. Setelah itu, pipe `des_p` biasa (tidak tersambung ke `STDOUT_FILENO` dan `STDIN_FILENO`) ditutup. Hal ini menjadikan output standar dari `execvp()` nanti akan masuk ke pipe dan akan dibaca oleh child process kedua dengan `scanf()`.
```c
        // Piping
        int des_p[2];
        if(pipe(des_p) == -1) {
            perror("Pipe failed");
            exit(1);
        }

        if(fork() == 0)            
        {
            close(STDOUT_FILENO);  
            dup(des_p[1]);         
            close(des_p[0]);      
            close(des_p[1]);

            char *args[] = {"base64", "--decode", path, (char *) 0 };
            execvp("base64", args);
            perror("execvp of ls failed");
            exit(1);
        }

        if(fork() == 0)            
        {
            close(STDIN_FILENO);   
            dup(des_p[0]);  
            close(des_p[0]);       
            close(des_p[1]);       

            char arr[100];
            scanf("%[^\n]s", arr);

            if(i == 1)
            {
                FILE *fp = fopen(filename, "w");
                fprintf(fp, "%s\n", arr);
                fclose(fp);
            }
            else 
            {
                FILE *fp = fopen(filename, "a");
                fprintf(fp, "%s\n", arr);
                fclose(fp);
            }
            exit(1);
        }

        close(des_p[0]);
        close(des_p[1]);
        wait(0);
        wait(0);
    }

    char *arg_rm[] = {"rm", "-rf", arguments, (char *) 0 };
    forking_v(arg_rm);
}
```
Setelah output dari child process 1 dibaca oleh child process 2, child process 2 kemudian akan menuliskan hasil decode ke dalam file yang sesuai dengan tipe argumen ("quote" atau "music") menggunakan bantuan dari array of char `filename`. Setelah itu, semua file descriptor yang dibuat akan ditutup.

## Soal 1.c
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/tree/master/soal1)

**Deskripsi:**
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

**Pembahasan:**
Pertama, akan dijalankan sebuah child process yang bertujuan untuk membuat directory bernama hasil. Selanjutnya, dibuat fungsi `move()` yang kemudian akan dipanggil pada saat pembuatan thread untuk soal 3. Pada saat pembuatan thread yang akan dilempar ke fungsi `move()`, dipassingkan juga sebuah argumen berupa tipe "quote" atau "music" 
```c
// Kebutuhan Soal 1c
void *move(void *arg);
pthread_t mv_thread[2];

int main()
{
    // Soal 1c
    char *arg_hasil[] = {"mkdir", "hasil", (char *) 0 };
    forking_v(arg_hasil);

    int mv_err1 = pthread_create(&(mv_thread[0]), NULL, &move, (void*) &type_quote);
    if(mv_err1 != 0) { printf("\n can't create thread : [%s]",strerror(mv_err1)); }

    int mv_err2 = pthread_create(&(mv_thread[1]), NULL, &move, (void*) &type_music);
    if(mv_err2 != 0) { printf("\n can't create thread : [%s]",strerror(mv_err2)); }

    pthread_join(mv_thread[0], NULL);
    pthread_join(mv_thread[1], NULL);
}

void *move(void *arg)
{
    char *arguments = arg;
    strcat(arguments, ".txt");

    char *arg_move[] = {"mv", arguments, "hasil" ,(char *) 0 };
    forking_v(arg_move);
}
```
Pada fungsi `move()` ini, akan dilakukan sebuah forking untuk mengeksekusi perintah untuk memindahkan file .txt ke dalam folder hasil. 

## Soal 1.d
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/tree/master/soal1)

**Deskripsi:**
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

**Pembahasan:**
Untuk menyelesaikan soal 1d, dilakukan pembuatan child process dengan fungsi `forking_v()` dua kali. Yang pertama berfungsi untuk mengezip folder hasil dengan password mihinomenestzahrafayya. Kemudian, yang kedua berfungsi untuk menghapus directory hasil beserta isi dari folder tersebut.
```c
int main()
{
    char *arg_zip[] = {"zip", "-q", "-r", "--password", "mihinomenestzahrafayya", "hasil.zip", "hasil", (char *) 0 };
    forking_v(arg_zip);

    char *arg_rm[] = {"rm", "-rf", "hasil", (char *) 0 };
    forking_v(arg_rm);
}
```
## Soal 1.e
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/tree/master/soal1)

**Deskripsi:**
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

**Pembahasan:**
Dibuat fungsi `unzip_hasil()` yang berfungsi untuk melakukan unzip terhadap hasil.zip. Dibuat pula fungsi `buat_no()` yang berfungsi untuk membuat file no.txt. Kemudian, akan dibuat dua thread yang akan berjalan pada dua fungsi tersebut. Setelah kedua thread tersebut selesai, hasil.zip akan dihapus dan dibuat child process dua kali. Yang pertama adalah untuk mengezip folder hasil dan yang kedua adalah untuk menghapus folder hasil. Setelah itu, no.txt akan dihapus.
```c
// Kebutuhan Soal 1e
void *unzip_hasil(void *arg);
void *buat_no(void *arg);
pthread_t e_thread[2];

int main()
{
    // Soal 1e
    int e_err1 = pthread_create(&(e_thread[0]), NULL, &unzip_hasil, NULL);
    if(e_err1 != 0) { printf("\n can't create thread : [%s]",strerror(e_err1)); }

    int e_err2 = pthread_create(&(e_thread[1]), NULL, &buat_no, NULL);
    if(e_err2 != 0) { printf("\n can't create thread : [%s]",strerror(e_err2)); }

    pthread_join(e_thread[0], NULL);
    pthread_join(e_thread[1], NULL);

    remove("hasil.zip");
    char *arg_zip2[] = {"zip", "-q", "-r", "--password", "mihinomenestzahrafayya", "hasil.zip", "no.txt", "hasil", (char *) 0 };
    forking_v(arg_zip2);
    forking_v(arg_rm);
    remove("no.txt");
}

void *unzip_hasil(void *arg)
{
    char *args[] = {"unzip", "-q", "-P", "mihinomenestzahrafayya", "hasil", (char *) 0 };
    forking_v(args);
}

void *buat_no(void *arg)
{
    FILE *fp = fopen("no.txt", "w");
    fprintf(fp, "No");
    fclose(fp);
    pthread_exit(NULL);
}
```

#  Soal 2
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/tree/master/soal2)

**Deskripsi:**
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria.

## Soal 2.a
Source Code: 
[server](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Server/server.c)
[client](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Client/client.c)

**Deskripsi:**
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client.

**Pembahasan:**
Server akan menerima kiriman dari client saat pertama kali dijalankan. Apabila client mengirimkan 'register' maka masuk ke dalam proses register di server, begitupun dengan 'login'.

*server code*
```c
        if (strcmp(buffer, "register") == 0){

            // Username
            username:
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                char *username_input = buffer;

                int unique = 1;
                for (int i = 0; i<index_username; i++){
                    if (strcmp(username[i], username_input) == 0){
                        // printf("Compare Username = %s\t%s\n", username[i], username_input);
                        unique = 0;
                        char *msg = "Username sudah ada";
                        send(new_socket, msg, strlen(msg), 0);
                        goto username;
                    }
                }

                if (unique){
                    strcpy(username[index_username], username_input);
                    // printf("Username Added = %s\t%s\n", username[index_username], username_input);
                    index_username++;
                    char *msg = "Username Unique";
                    send(new_socket, msg, strlen(msg), 0);
                }

                printf("%s:", buffer);
                fprintf(writeFile, "%s:", username_input);

            //Password
            memset(buffer, 0, 1024);
            valread = read(new_socket, buffer, 1024);
            char *password_input = buffer;
            printf("%s\n", buffer);
            fprintf(writeFile, "%s\n", password_input);
            
        }

        else if ((strcmp(buffer, "login") == 0) && (user == 0)){
            FILE *readFile;
            login_username:
                readFile = fopen("users.txt", "r");
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);

                int exist = 0;
                char data[1024];
                char *check;
                while(!feof(readFile)){
                    fscanf(readFile, "%s", data);
                    check = strtok(data, ":");
                    if (strcmp(buffer, check) == 0){
                        exist = 1;
                        strcpy(login_username, check);
                        check = strtok(NULL, ":");
                        printf("Password = %s\n", check);
                        break;
                    }
                }
                if (exist == 0){
                    char *msg = "Username tidak ada";
                    send(new_socket, msg, strlen(msg), 0);
                    fclose(readFile);
                    goto login_username;
                }else{
                    char *msg = "Username ada";
                    send(new_socket, msg, strlen(msg), 0);
                }
                fclose(readFile);


            login_password:
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                if (strcmp(buffer, check) != 0){
                    char *msg = "Password Salah";
                    send(new_socket, msg, strlen(msg), 0);
                    goto login_password;
                }else{
                    login_successful = 1;
                    char *msg = "Login Berhasil\n";
                    send(new_socket, msg, strlen(msg), 0);
                    user++;
                }
        }
```

* Setiap register dilakukan pengecekan username. Apabila username dari client sudah ada maka tidak bisa dilanjutkan/
* Apabila username dari client belum ada maka register dapat dilanjutkan
* Saat login, username dari client harus username yang tersedia di users.txt dan saat memasukan password di cek kesesuaian dengan yang ada di file users.txt

*client code*
```c
        if (strcmp(input, "register") <= 0){

            username:
                memset(buffer, 0, 1024);
                printf("Username: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Username sudah ada") == 0){
                    printf("%s\n", buffer);
                    goto username;
                }else
                    printf("%s\n", buffer);

            int uppercase, lowercase, number;
            password:
                memset(buffer, 0, 1024);
                uppercase = 0;
                lowercase = 0;
                number = 0;
                printf("Password: ");
                scanf("%s", input);
                if (strlen(input) < 6){
                    printf("\nPassword minimal terdiri dari 6 karakter\n\n");
                    goto password;
                }
                for (int i = 0; i<strlen(input); i++){
                    // printf("%c ", input[i]);
                    if (input[i]>=48 && input[i]<=57)
                        number = 1;
                    if (input[i]>=65 && input[i]<=90)
                        uppercase = 1;
                    if (input[i]>=97 && input[i]<=122)
                        lowercase = 1;
                    // printf("number = %d, uppercase = %d, lowercase = %d\n", number, uppercase, lowercase);
                }
                if (number == 0 || uppercase == 0 || lowercase == 0){
                    printf("\nPassword minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil\n\n");
                    goto password;
                }
                send(sock, input, strlen(input), 0);
                printf("Akun terdaftar\n");
        }

        else if (strcmp(input, "login") <= 0){
            
            login_username:
                memset(buffer, 0, 1024);
                printf("Username: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Username tidak ada") == 0){
                    printf("%s\n", buffer);
                    goto login_username;
                }

            login_password:
                memset(buffer, 0, 1024);
                printf("Password: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);
                valread = read(sock, buffer, 1024);
                if (strcmp(buffer, "Password Salah") == 0){
                    printf("%s\n", buffer);
                    goto login_password;
                }else
                    printf("%s\n", buffer);
        }
```

* Pengecekan syarat password dilakukan di sisi client karena tidak memerlukan data yang ada di server.

## Soal 2.b & 2.c
Source Code: 
[server](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Server/server.c)
[client](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Client/client.c)

**Deskripsi:**
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama *problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan. Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem

**Pembahasan:**
Fitur add akan bisa dilakukan ketika user sudah login. Hal ini ditandai dengan variabel *login_successful* dan *user*.

*server code*
```c
        else if ((strcmp(buffer, "add") <= 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Add Section";
                send(new_socket, msg, strlen(msg), 0);

                pthread_t tid[4];

                add_judul:
                    memset(buffer, 0, 1024);
                    valread = read(new_socket, buffer, 1024);
                    printf("Judul Problem: %s\t", buffer);
                    char *foldername_input = buffer;
                    int unique = 1;
                    for (int i = 0; i<index_judul; i++){
                        if (strcmp(username[i], foldername_input) == 0){
                            unique = 0;
                            char *msg = "Judul Problem sudah ada";
                            send(new_socket, msg, strlen(msg), 0);
                            goto add_judul;
                        }
                    }
                    if (unique){
                        strcpy(temp_judul, buffer);
                        strcpy(judul[index_judul], foldername_input);
                        index_judul++;

                        char *msg = "Judul Problem Unique";
                        send(new_socket, msg, strlen(msg), 0);

                        pthread_create(&tid[0], NULL, &make_directory, buffer);
                        pthread_join(tid[0], NULL);
                    }
                

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath description.txt: %s\n", buffer);
                pthread_create(&tid[1], NULL, &add_description_file, buffer);
                pthread_join(tid[1], NULL);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath input.txt: %s\n", buffer);
                pthread_create(&tid[2], NULL, &add_input_file, buffer);
                pthread_join(tid[2], NULL);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                printf("Filepath output.txt: %s\n", buffer);
                pthread_create(&tid[3], NULL, &add_output_file, buffer);
                pthread_join(tid[3], NULL);

                fprintf(database, "%s \t %s\n", temp_judul, login_username);
            }
        }
```

*client code*
```c
        else if (strcmp(input, "add") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);

            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                add_judul:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);
                    valread = read(sock, buffer, 1024);
                    if (strcmp(buffer, "Judul Problem sudah ada") == 0){
                        printf("%s\n", buffer);
                        goto add_judul;
                    }else{
                        printf("%s\n", buffer);
                    }
                
                add_filepath_description:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);

                add_filepath_input:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);

                add_filepath_output:
                    memset(buffer, 0, 1024);
                    scanf("%s", input);
                    send(sock, input, strlen(input), 0);
    
            }
        }
```

* Judul problem harus dicek terlebih dahulu karena harus unique
* Input nama file harus beserta dengan extensinya
* Diakhir tambahkan judul dan username je database *problem.tsv*

## Soal 2.d
Source Code: 
[server](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Server/server.c)
[client](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Client/client.c)

**Deskripsi:**
Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut)

**Pembahasan:**
Penyelesaian dilakukan dengan membaca file problems.tsv dan diatur format outputnya di client

*server code*
```c
        else if ((strcmp(buffer, "see") == 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                memset(buffer, 0, 1024);
                char response[1024];
                char output[1024];
                memset(response, 0, sizeof(response));
                memset(output, 0, sizeof(output));

                int i = 1;
                while(fgets(response, sizeof(response), database) != NULL){
                    strcat(output, response);
                }
                memset(buffer, 0, 1024);
                send(new_socket, output, strlen(output), 0);
            }
        }

```

*client code*
```c
        else if (strcmp(input, "see") == 0){
            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                memset(buffer, 0, 1024);
                valread = read(sock, buffer, 1024);
                char output[256];
                char temp[256];

                memset(output, 0, sizeof(output));
                memset(temp, 0, sizeof(temp));

                strcpy(temp, buffer);

                for (int i = 0; i<strlen(temp); i++){
                    if (temp[i] == ' ') continue;
                    else if (temp[i+1] == ' ' && (temp[i] != ' ' && temp[i] != '\t')){
                        strncat(output, &temp[i], 1);
                        strcat(output, " by ");
                        continue;
                    }
                    strncat(output, &temp[i], 1);
                }
                printf("%s\n", output);
            }
            
        }

```

## Soal 2.e
Source Code: 
[server](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Server/server.c)
[client](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Client/client.c)

**Deskripsi:**
Client yang telah login, dapat memasukkan command ‘download judul-problem’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu judul-problem. Kedua file tersebut akan disimpan ke folder dengan nama judul-problem di client.

**Pembahasan:**
Proses download diawali dengan membaca file yang ada di server lalu di copy ke client. Berikut code dalam implementasi download

*server code*
```c
        else if ((strcmp(buffer, "download") == 0) && (user == 0)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Download Section";
                send(new_socket, msg, strlen(msg), 0);
                pthread_t tid;
                
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                pthread_create(&tid, NULL, &download_file, buffer);
                pthread_join(tid, NULL);
            }
        }
```

*client code*
```c
        else if (strcmp(input, "download") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);

            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                printf("%s\n", buffer);
                memset(buffer, 0, 1024);
                printf("Judul Problem: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);
            }
        }
```

## Soal 2.f
Source Code: 
[server](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Server/server.c)
[client](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Client/client.c)


**Deskripsi:**
Client yang telah login, dapat memasukkan command ‘submit judul-problem path-file-output.txt’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.


**Pembahasan:**
Penyelesaian dilakukan dengan read kedua file dan membandingkan tiap baris

*server code*
```c
        else if ((strcmp(buffer, "submit") == 0) && (user <= 1)){
            if (login_successful == 0){
                char *msg = "Mohon Login Terlebih Dahulu";
                send(new_socket, msg, strlen(msg), 0);
            }
            else{
                char *msg = "Submit Section";
                send(new_socket, msg, strlen(msg), 0);
                
                char judul_input[100], output_input[100], client_path[100];
                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                strcpy(judul_input, buffer);
                printf("Judul Problem = %s\n", judul_input);

                memset(buffer, 0, 1024);
                valread = read(new_socket, buffer, 1024);
                strcpy(output_input, buffer);
                printf("Output File = %s\n", output_input);


                pthread_t tid;
                strcpy(client_path, "../Client/");
                strcat(client_path, output_input);
                strcpy(temp_judul, judul_input);
                pthread_create(&tid, NULL, &check_submit, client_path);
                pthread_join(tid, NULL);

                if (ac == 1){
                    memset(buffer, 0, 1024);
                    send(new_socket, "AC", strlen("AC"), 0);
                }else{
                    memset(buffer, 0, 1024);
                    send(new_socket, "WA", strlen("WA"), 0);
                    ac = 1;
                }
            }
        }
```

*client code*
```c
        else if (strcmp(input, "submit") == 0){
            memset(buffer, 0, 1024);
            valread = read(sock, buffer, 1024);
            if (strcmp(buffer, "Mohon Login Terlebih Dahulu") == 0){
                printf("%s\n", buffer);
            }else{
                printf("Judul Problem: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                printf("Output File: ");
                scanf("%s", input);
                send(sock, input, strlen(input), 0);

                memset(buffer, 0, 1024);
                valread = read(sock, buffer, 1024);
                printf("%s\n\n", buffer);
                
            }
        }

```

## Soal 2.g
Source Code: 
[server](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Server/server.c)
[client](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/blob/master/soal2/Client/client.c)


**Deskripsi:**
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

**Pembahasan:**
Penyelesaian dilakukan dengan queue dan mengatur agar client lain tidak bisa mengakses socket server (login) apabila client pertama belum keluar dari server (log out)

*server code*
```c
    int queue[3];
    int count = 0;

    void insertIntoQueue(int x){
        if (count == 3){
            fprintf(stderr, "No more space for client\n");
        }
        queue[count] = x;
        count++;
    }

    int removeFromQueue(){
        if (count == 0){
            fprintf(stderr, "No elements to extract from queue\n");
            return -1;
        }
        int res = queue[0];
        int i;
        for (i = 0; i<count-1; i++){
            queue[i] = queue[i+1];
        }
        count--;
        return res;
    }

        ...

        if (FD_ISSET(server_fd, &readfds) && user < 1){
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address,(socklen_t *)&addrlen)) < 0){
                 perror ("accept");
                 exit(EXIT_FAILURE);
            }

            printf("New connection, socket fd is %d, ip is: %s, port: %d\n", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));
            user++;
            insertIntoQueue(new_socket);

            for (i = 0; i< max_clients; i++){
                if (client_socket[i] == 0){
                    client_socket[i] = new_socket;
                    break;
                }
            }
        }
```

#  Soal 3
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-3-d10-2022/-/tree/master/soal3)

**Deskripsi:**
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan

## Soal 3.a dan 3.b
**Deskripsi:**
a.	Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

b.	Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

c.  Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

**Pembahasan:**
Pembuatan fungsi-fungsi untuk pengkategorian jenis file. 

```c
//cek eksistensi file
int fileifexists(const char *filename){
    struct status buffer;
    int exists = status(filename, &buffer);
    if (exists == 0)
        return 1;
    else return 0;
}

//file moving ke folder sesuai kategori
void *filemove(void *filename){
    char cwd[PATH_MAX];
    char dirname[100];
    char hidden[100];
    char file[100];
    char hiddenfilename[100];
    char fileexists[100];
    
    int i;

    strcpy(fileexists, filename);
    strcpy(hiddenfilename, filename);
    
    char *name = strchr(hiddenfilename, '/');
    strcpy(hidden, name);

//file biasa
    if (strstr(filename, ".") != NULL){
        strcpy(file, filename);
        strtok(file, ".");
        char *tok = strtok(NULL, "");
    
        for(i = 0; tok[i]; i++){
            tok[i] = tolower(tok[i]);
        }
            strcpy(dirname, tok);
    }
//file yang hidden dengan awalan '.'
    else if(hidden[1] == '.'){
        strcpy(dirname, "Hidden");
    }

//file tanpa ekstensi
    else{
        strcpy(dirname, "Unknown");
    }

//pembuatan directory sesuai nama atau ekstensi file
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        char *foldername = strchr(filename, '/');
        char namefile[100];

        strcpy(namefile, cwd);
        strcat(namefile, "/");
        strcat(namefile, dirname);
        strcat(namefile, foldername);
        rename(filename, namefile);
    }

}

//listing kategori secara rekursif 
void filelisting(char *pathbase){
    char path[100];
    struct dirent *dp;
    DR *dir = opendir(pathbase);

    struct status buffer;

    int n = 0;

    if(dp != NULL){
        if (strcmp(dp->dir_name, ".") != 0 && strcmp(dp->dir_name, ". .") != 0){
            
            strcpy(path, pathbase);
            strcat(path, "/");
            strcat(path, dp->dir_name);

            if (status(path, &buffer) == 0 && S_ISREG(buffer.st_mode){
                pthread_c thread;
                int thr = create_pthread(&thread, NULL, filemove, (void *)path);
                pthread_join(thread, NULL);
            }
            filelisting(path);
        }
    }
    closedir(dir);
}

int main(int argc, char *argv[]){
    
    char cwd[PATH_MAX];
    char path[100];
    
    if(strcmp(argv[1], "-f") == 0)
    {
    int i;
    pthread_t thread;

    for(i = 2; i < argc; i++)
    {
        char printmsg[100];
        int exists = fileifexists(argv[i]);
        if(exists)
        {
            sprintf(printmsg, "File %d: Categorized!", i - 1 );
        }
        else
        {
            sprintf(printmsg, "File %d: Failed to Categorize", i - 1);
        }
        printf("%s\n", printmsg);
        int err = pthread_create(&thread, NULL, filemove, (void *)path);   

    }
 
    pthread_join(thread, NULL);
    }
    else
    {
        if(strcmp(argv[1], "*") == 0)
        {
            if(getcwd(cwd, sizeof(cwd)) != NULL)
            {
                filelisting(cwd);
            }
        }
        
        else if(strcmp(argv[1], "d") == 0)
        {
            filelisting(argv[2]);
            struct stat buffer;
            int err = stat(argv[2], &buffer);
            if(err == -1)
            {
                printf("Failed to save\n");
            }
            else
            {
                printf("Directory saved\n");
            }
        }
    }

return 0;
}

```

## Soal 3.d dan 3.e 
**Deskripsi:** 
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command "send hartakarun.zip".

**Pembahasan:**
Menggunakan socket client-server untuk pengiriman file hartakarun.zip. 

*client code* 
```c
char folder_directory[100] = "hartakarun";

int sender(int socket, char *filename);
int connector(char *filename);
void zipper();


int main(int argc, char const *argv[])
{   
    zipper();
    return 0;
}

//fungsi pengirim file
int sender(int socket, char *filename)
{
    
    char fpath[1024], buffer[1024] = {0};
    int sizeFile;
    int bufferOnset();

    strcpy(fpath, "./");
    strcat(fpath, filename);
    FILE *file = fopen(fpath, "r");
    
    if (file != NULL)
    {
        bufferOnset(buffer, 1024);
    } else {
        printf("File not found: %s\n", filename);
        return -1;
    }

    sizeFile = fread(buffer, sizeof (char));

    while ((sizeFile, 1024, file) > 0)
    {
        if (send(socket, buffer, sizeFile, 0) < 0)
        {
            fprintf(stderr, "Failed to send: %s.\n", filename);
        }
        (buffer, 1024);
    }
    fclose(file);
    return 0;
}

//client connection maker dan checker
int connector(char *filename){

    int socketCheck;
    struct sockaddr_in address, serverAddress;
    int sock = 0, valread;
    char *hello = "Client Requests", buffer[1024] = {0};

    socketCheck = socket(AF_INET, SOCK_STREAM, 0);
    if (socketCheck < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serverAddress, '0', sizeof(serverAddress));

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(socketCheck, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

//memanggil fungsi pengirim file
    send(socketCheck , hello , strlen(hello) , 0 );
    sender(sock, filename);
    printf("\nFile Sent\n");

    return 0;
}

//fungsi zip untuk men-zip file hartakarun.
void zipper(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        int status = 0;
        waitpid(child_id, &status, 0);
    }

    if (child_id == 0) {
        char *argv[]={"zip","-mr","hartakarun.zip","/soal3/hartakarun", NULL};
        execv("/usr/bin/zip",argv);
    } else {
        while ((wait(&status)) > 0);
        connector("hartakarun.zip");
    }
    
}

```

*server code*
```c
int receiver(int socket, char *filename);

int main (int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    int numbers[100];

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    valread = read(new_socket, buffer, 1024);
    receiver(new_socket, "hartakarun.zip");
    close(new_socket);

    return 0;
}

//fungsi penerima file
int receiver(int socket, char *filename){
    char buffer[1024] = {0}, fpath[1024];
    int fileSize = 0;
    int bufferOnset();
   
    strcpy(fpath, "./");
    strcat(fpath, filename);
    FILE *read_file = fopen(fpath, "wb");

    if (read_file == NULL)
    {
        printf("File not found: %s\n", filename);
        return -1;
    }
    else {
        printf("File from Client: %s\n", filename);
        bufferOnset(buffer, 1024);
        fileSize = recv(socket, buffer, 1024, 0);

        while (fileSize > 0)
        {
            int size_write = fwrite(buffer, sizeof(char), fileSize, read_file);
            if (size_write < fileSize)
            {
                printf("Server failed to keep\n");
                return 0;
            }

            bufferOnset(buffer, 1024);

            if (fileSize == 0)
            {
                break;
            }
        }
    }
    fclose(read_file);
}
```


















